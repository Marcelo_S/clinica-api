const express = require("express");
const bodyParse = require("body-parser");
const dotEnv = require("dotenv");
const morgan = require("morgan");
const cookieParser = require("cookie-parser");
const cors = require("cors");
const swaggerJSDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");

const knexConfig = require("./knexfile");
const Knex = require("knex");
const { Model } = require("objection");

dotEnv.config({ path: "./config.env" });

const verifyToken = require("./middleware/verifyToken");
const permissions = require("./middleware/permissions");

const optionsController = require("./controllers/routesController");
const usersController = require("./controllers/usersController");
const rolesController = require("./controllers/rolesController");
const optionsRole = require("./controllers/routesRoleController");
const EmergencyContact = require("./controllers/emergencyContactController");
const Patient = require("./controllers/PatientController");
const MedicalAppointment = require("./controllers/MedicalAppointmentController");
const MedicalHistory = require("./controllers/medicalHistoryController");

const knex = Knex(knexConfig.development);
Model.knex(knex);

const app = express();
app.use(cors());
app.use(morgan("dev"));
app.use(cookieParser());
app.use(bodyParse.json());
app.use(bodyParse.urlencoded({ extend: false }));

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );

  if (req.method === "OPTIONS") {
    res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
    return res.status(200).json({});
  }

  next();
});

// swagger documentation
const swaggerDefinition = {
  info: {
    title: "Startup Node JS API",
    version: "1.0.0",
    description: "Started API documentation",
    host: `${process.env.HOST}:${process.env.PORT}`,
    basePath: "/",
  },
};

const swaggerOptions = {
  swaggerDefinition,
  apis: ["./controllers/*.js"],
};

const swaggerSpec = swaggerJSDoc(swaggerOptions);

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerSpec));

// define static folder
app.use("/uploads", express.static("uploads"));

app.use("/options", optionsController);
app.use("/users", usersController);
app.use("/roles", rolesController);
app.use("/optionsRole", optionsRole);
app.use("/emergency-contact", EmergencyContact);
app.use("/patients", Patient);
app.use("/medical-appointments", MedicalAppointment);
app.use("/medical-histories", MedicalHistory);

app.use((_req, res) => {
  res.status(404).send({ error: "page not found" });
});

app.use((err, _req, res) => {
  res.status(err.status || 500);
  res.send({
    message: err.message,
    errors: err.errors,
  });
});

app.listen(process.env.PORT || 3000, (_) => {
  console.clear();
  console.log(`Server is running on port ${process.env.PORT || 3000}`);
});
