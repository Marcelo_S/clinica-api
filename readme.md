# run latest migration

knex migrate:latest

# run latest seed

knex seed:run
