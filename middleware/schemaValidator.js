const validator = (schema) => (req, res, next) => {
  const { error } = schema.validate(req.body);

  if (!error) return next();

  const { details } = error;
  const message = details
    .map((i) => i.message)
    .join(",")
    .split('"')
    .join("");

  return res.status(422).send({ message, status: 422 });
};

module.exports = validator;
