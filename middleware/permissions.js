const User = require("../models/usersModel");

const permissions = async (req, res, next) => {
  const idUser = req.user_id;

  if (!idUser) {
    return res.status(403).send({ error: "Unauthorized" });
  }

  const user = new User();
  const options = await user.findPermissions(idUser);
  const url = req.originalUrl.match(/\/[a-zA-Z0-9]+/g).join();

  const allow = options.find((o) => o === url);

  if (!allow) {
    return res.status(403).send({ error: "Unauthorized" });
  }

  next();
};

module.exports = permissions;
