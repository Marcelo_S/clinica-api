const asyncWrap = (fn) =>
  function asyncUtilWrap(req, res, next, ...arg) {
    const fnReturn = fn(req, res, next, ...arg);
    return Promise.resolve(fnReturn).catch(next);
  };

module.exports = asyncWrap;
