const joi = require("joi");

const schema = {
  roleCreation: joi.object({
    description: joi.string().required(),
    is_active: joi.boolean().optional(),
  }),
};

module.exports = schema;
