// Update with your config settings.

module.exports = {
  development: {
    client: "mysql",
    connection: {
      database: "clinica_db",
      user: "root",
      password: "",
    },
    migrations: {
      directory: __dirname + "/migrations",
      tableName: "knex_migrations",
    },
    seeds: {
      directory: __dirname + "/seeds",
    },
  },

  staging: {
    client: "mysql",
    connection: {
      database: "clinica_db",
      user: "root",
      password: "123",
    },
    migrations: {
      directory: __dirname + "/migrations",
      tableName: "knex_migrations",
    },
  },

  production: {
    client: "mysql",
    connection: {
      database: "clinica_db",
      user: "root",
      password: "123",
    },
    migrations: {
      directory: __dirname + "/migrations",
      tableName: "knex_migrations",
    },
  },
};
