exports.seed = function (knex) {
  return knex("routes")
    .del()
    .then(function () {
      return knex("routes").insert([
        // #region route
        { description: "fetch all routes", is_active: true, route: "/route" },
        {
          description: "Create Options",
          is_active: true,
          route: "/route/create",
        },
        {
          description: "Update Options",
          is_active: true,
          route: "/route/update",
        },
        {
          description: "Delete Options",
          is_active: true,
          route: "/route/delete",
        },
        // #endregion route

        // #region route
        { description: "fetch all Users", is_active: true, route: "/users" },
        {
          description: "Create Users",
          is_active: true,
          route: "/users/create",
        },
        {
          description: "Update Users",
          is_active: true,
          route: "/users/update",
        },
        {
          description: "Delete Users",
          is_active: true,
          route: "/users/delete",
        },
        // #endregion route

        // #region route
        { description: "fetch all Roles", is_active: true, route: "/roles" },
        {
          description: "Create Roles",
          is_active: true,
          route: "/roles/create",
        },
        {
          description: "Update Roles",
          is_active: true,
          route: "/roles/update",
        },
        {
          description: "Delete Roles",
          is_active: true,
          route: "/roles/delete",
        },
        // #endregion route
      ]);
    });
};
