exports.seed = function (knex) {
  return knex("roles")
    .del()
    .then(function () {
      return knex("roles").insert([
        { description: "Admin", is_active: true },
        { description: "Content Creator", is_active: true },
        { description: "Data Recorder", is_active: true },
      ]);
    });
};
