exports.seed = function (knex) {
  return knex("users")
    .del()
    .then(function () {
      return knex("users").insert([
        {
          id_rol: 1, // admin role
          first_name: "Engel",
          middle_name: "Enrique",
          first_surname: "Pena",
          second_surname: "padilla",
          username: "Yunita",
          password: "123",
          description: "This is a user test",
        },
      ]);
    });
};
