const { Model } = require("objection");

class Routes extends Model {
  static get idColumn() {
    return "id";
  }

  static get tableName() {
    return "routes";
  }

  static get jsonSchema() {
    return {
      type: "object",
      required: ["description"],

      properties: {
        description: { type: "string" },
      },
    };
  }

  create = async (data) => {
    try {
      await Routes.query().insert(data);
      return true;
    } catch (error) {
      return false;
    }
  };

  update = async (id, data) => {
    try {
      await Routes.query().findById(id).patch(data);
      return true;
    } catch (error) {
      return false;
    }
  };

  delete = async (id) => {
    try {
      await Routes.query().findById(id).patch({
        is_active: 0,
        deleted_at: new Date(),
      });

      return true;
    } catch (error) {
      return false;
    }
  };

  find = async (id) => {
    const result = await Routes.query().findById(id);

    return result;
  };

  fetchAll = async (page = "", limit = "", text = "") => {
    if (parseInt(page) > 0 && parseInt(limit) > 0) {
      const { results, total } = await Routes.query()
        .where("description", "like", `%${text}%`)
        .page(page - 1, limit);

      const pageNumbers = Math.ceil(total / limit);

      return { result: results, pageNumbers };
    }

    const options = await Routes.query().where(
      "description",
      "like",
      `%${text}%`
    );
    return { result: options };
  };

  async $beforeInsert(queryContext) {
    await super.$beforeInsert(queryContext);
    this.created_at = new Date();
  }

  async $beforeUpdate(opt, queryContext) {
    await super.$beforeInsert(opt, queryContext);
    this.updated_at = new Date();
  }
}

module.exports = Routes;
