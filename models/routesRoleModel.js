const { Model } = require("objection");

class RoutesRole extends Model {
  static get idColumn() {
    return "id";
  }

  static get tableName() {
    return "route_role";
  }

  static get jsonSchema() {
    return {
      type: "object",
      required: ["id_role", "id_route"],

      properties: {
        id_role: { type: "integer" },
        id_option: { type: "integer" },
      },
    };
  }

  fetchAll = async () => {
    return await RoutesRole.query();
  };

  create = async (data) => {
    try {
      await RoutesRole.query().insert(data);
      return true;
    } catch (error) {
      return false;
    }
  };

  update = async (id, data) => {
    try {
      await RoutesRole.query().findById(id).patch(data);
      return true;
    } catch (error) {
      return false;
    }
  };

  delete = async (id) => {
    try {
      await RoutesRole.query().findById(id).patch({
        is_active: 0,
        deleted_at: new Date(),
      });

      return true;
    } catch (error) {
      return false;
    }
  };

  find = async (id) => {
    const result = await RoutesRole.query().findById(id);

    return result;
  };

  async $beforeInsert(queryContext) {
    await super.$beforeInsert(queryContext);
    this.created_at = new Date();
  }

  async $beforeUpdate(opt, queryContext) {
    await super.$beforeInsert(opt, queryContext);
    this.updated_at = new Date();
  }
}

module.exports = RoutesRole;
