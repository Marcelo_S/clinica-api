const { Model, fn, ref } = require("objection");

class Patient extends Model {
  static get idColumn() {
    return "id";
  }

  static get tableName() {
    return "patients";
  }

  static get jsonSchema() {
    return {
      type: "object",
      required: [
        "first_name",
        "last_name",
        "age",
        "birthday",
        "gender",
        "address",
        "city",
        "cellphone",
        "mail_address",
        "martial_status",
        "profession",
        "emergency_contact_id",
      ],
      properties: {
        first_name: { type: "string" },
        last_name: { type: "string" },
        age: { type: "string" },
        birthday: { type: "date" },
        gender: { type: "integer" },
        address: { type: "string" },
        city: { type: "string" },
        cellphone: { type: "string" },
        mail_address: { type: "string" },
        martial_status: { type: "string" },
        profession: { type: "string" },
        emergency_contact_id: { type: "integer" },
      },
    };
  }

  static get relationMappings() {
    const EmergencyContact = require("./emergencyContactModel");
    const MedicalHistory = require("./MedicalHistoryModel");

    return {
      emergencyContact: {
        relation: Model.BelongsToOneRelation,
        modelClass: EmergencyContact,

        join: {
          from: "patients.emergency_contact_id",
          to: "emergency_contact.id",
        },
      },
      medicalHistory: {
        relation: Model.HasManyRelation,
        modelClass: MedicalHistory,

        join: {
          from: "patients.id",
          to: "medical_history.patient_id",
        },
      },
    };
  }

  fetchAll = async (page = "", limit = "", text = "") => {
    if (parseInt(page) > 0 && parseInt(limit) > 0) {
      const { results, total } = await Patient.query()
        .where("first_name", "like", `%${text}%`)
        .orWhere("last_name", "like", `%${text}%`)
        .orWhere("cellphone", "like", `%${text}%`)
        .orWhere(
          fn.concat(ref("first_name"), " ", ref("last_name")),
          "like",
          `%${text}%`
        )
        .page(page - 1, limit)
        .withGraphFetched({ emergencyContact: true, medicalHistory: true });

      const pageNumbers = Math.ceil(total / limit);

      return { result: results, pageNumbers };
    }

    const emergencyContact = await Patient.query()
      .where("first_name", "like", `%${text}%`)
      .orWhere("last_name", "like", `%${text}%`)
      .orWhere("cellphone", "like", `%${text}%`)
      .orWhere(
        fn.concat(ref("first_name"), " ", ref("last_name")),
        "like",
        `%${text}%`
      )
      .withGraphFetched({ emergencyContact: true, medicalHistory: true });

    return { result: emergencyContact };
  };

  create = async (data) => {
    try {
      await Patient.query().insert(data);
      return true;
    } catch (error) {
      console.log({
        error,
      });
      return false;
    }
  };

  update = async (id, data) => {
    try {
      await Patient.query().findById(id).patch(data);
      return true;
    } catch (error) {
      console.log("error", error)
      return false;
    }
  };

  delete = async (id) => {
    try {
      await Patient.query().findById(id).patch({
        is_active: 0,
        deleted_at: new Date(),
      });

      return true;
    } catch (error) {
      return false;
    }
  };

  find = async (id) => {
    const result = await Patient.query().findById(id).witn;

    return result;
  };

  async $beforeInsert(queryContext) {
    await super.$beforeInsert(queryContext);
    this.created_at = new Date();
  }

  async $beforeUpdate(opt, queryContext) {
    await super.$beforeInsert(opt, queryContext);
    this.updated_at = new Date();
  }
}

module.exports = Patient;
