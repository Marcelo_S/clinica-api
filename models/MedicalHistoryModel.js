const { Model } = require("objection");
const { v4: uuid } = require("uuid");

class MedicalHistory extends Model {
  static get idColumn() {
    return "id";
  }

  static get tableName() {
    return "medical_history";
  }

  static get jsonSchema() {
    return {
      type: "object",
      required: ["reason_admission", "health_condition", "patient_id"],
      properties: {
        identifier: { type: "string" },
        reason_admission: { type: "string" },
        health_condition: { type: "string" },
        is_active: { type: "boolean" },
        patient_id: { type: "integer" },
      },
    };
  }

  static get relationMappings() {
    const Patient = require("./patientModel");

    return {
      patient: {
        relation: Model.BelongsToOneRelation,
        modelClass: Patient,

        join: {
          from: "medical_history.patient_id",
          to: "patients.id",
        },
      },
    };
  }

  fetchAll = async (page = "", limit = "") => {
    if (parseInt(page) > 0 && parseInt(limit) > 0) {
      const { results, total } = await MedicalHistory.query()
        .page(page - 1, limit)
        .withGraphFetched({ patient: true });

      const pageNumbers = Math.ceil(total / limit);

      return { result: results, pageNumbers };
    }

    const medicalAppointment = await MedicalHistory.query().withGraphFetched({
      patient: true,
    });

    return { result: medicalAppointment };
  };

  create = async (data) => {
    try {
      await MedicalHistory.query().insert({
        ...data,
        identifier: uuid(),
      });

      return true;
    } catch (error) {
      console.log({
        error,
      });
      return false;
    }
  };

  update = async (id, data) => {
    try {
      const result = await MedicalHistory.query().findById(id).patch(data);
      return true;
    } catch (error) {
      return false;
    }
  };

  delete = async (id) => {
    try {
      await MedicalHistory.query().findById(id).patch({
        is_active: 0,
        deleted_at: new Date(),
      });

      return true;
    } catch (error) {
      return false;
    }
  };

  find = async (id) => {
    const result = await MedicalHistory.query()
      .findById(id)
      .withGraphFetched({ patient: true });

    return result;
  };

  async $beforeInsert(queryContext) {
    await super.$beforeInsert(queryContext);
    this.created_at = new Date();
  }

  async $beforeUpdate(opt, queryContext) {
    await super.$beforeInsert(opt, queryContext);
    this.updated_at = new Date();
  }
}

module.exports = MedicalHistory;
