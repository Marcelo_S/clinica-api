const { Model, fn, raw, ref } = require("objection");
const bcrypt = require("bcrypt");

class User extends Model {
  static get idColumn() {
    return "id";
  }

  static get tableName() {
    return "users";
  }

  static get jsonSchema() {
    return {
      type: "object",
      required: [
        "id_rol",
        "first_name",
        "middle_name",
        "first_surname",
        "second_surname",
        "username",
        "password",
        "description",
      ],

      properties: {
        id_rol: { type: "integer" },
        first_name: { type: "string" },
        middle_name: { type: "string" },
        first_surname: { type: "string" },
        second_surname: { type: "string" },
        username: { type: "string" },
        password: { type: "string" },
        description: { type: "string" },
        created_at: { type: "date" },
        updated_at: { type: "date" },
      },
    };
  }

  static get relationMappings() {
    const Publication = require("./publicationsModel");
    const Rol = require("./rolesModel");

    return {
      publication: {
        relation: Model.HasManyRelation,
        modelClass: Publication,

        join: {
          from: "users.id",
          to: "publications.id_users",
        },
      },
      rol: {
        relation: Model.BelongsToOneRelation,
        modelClass: Rol,

        join: {
          from: "users.id_rol",
          to: "roles.id",
        },
      },
    };
  }

  create = async (user) => {
    try {
      const bcPassword = await bcrypt.hash(user.password, 10);
      await User.query().insert({
        ...user,
        password: bcPassword,
        id_rol: parseInt(user.id_rol),
      });

      return true;
    } catch (error) {
      console.log("error: ", error);
      return false;
    }
  };

  update = async (user, id) => {
    try {
      let toUpdate = {};

      if (user.password) {
        const bcPassword = await bcrypt.hash(user.password, 10);
        toUpdate = {
          ...user,
          password: bcPassword,
          id_rol: parseInt(user.id_rol),
        };
      } else {
        toUpdate = { ...user, id_rol: parseInt(user.id_rol) };
      }

      await User.query().findById(id).patch(toUpdate);

      return true;
    } catch (error) {
      return false;
    }
  };

  delete = async (id) => {
    try {
      await User.query().findById(id).patch({
        is_active: 0,
        deleted_at: new Date(),
      });

      return true;
    } catch (error) {
      console.log(error);
      return false;
    }
  };

  find = async (id) => {
    const result = await User.query().findById(id);
    return result;
  };

  fetchAll = async (page = "", limit = "", text = "") => {
    if (parseInt(page) > 0 && parseInt(limit) > 0) {
      const { results, total } = await User.query()
        .where("first_name", "like", `%${text}%`)
        .orWhere("middle_name", "like", `%${text}%`)
        .orWhere("first_surname", "like", `%${text}%`)
        .orWhere("second_surname", "like", `%${text}%`)
        .orWhere("username", "like", `%${text}%`)
        .orWhere(
          fn.concat(
            ref("first_name"),
            " ",
            ref("middle_name"),
            " ",
            ref("first_surname"),
            " ",
            ref("second_surname")
          ),
          "like",
          `%${text}%`
        )
        .page(page - 1, limit)
        .withGraphFetched({ publication: true, rol: true });

      const pageNumbers = Math.ceil(total / limit);

      return { result: results, pageNumbers };
    }
    const user = await User.query()
      .where("first_name", "like", `%${text}%`)
      .orWhere("middle_name", "like", `%${text}%`)
      .orWhere("first_surname", "like", `%${text}%`)
      .orWhere("second_surname", "like", `%${text}%`)
      .orWhere("username", "like", `%${text}%`)
      .orWhere(
        fn.concat(
          ref("first_name"),
          " ",
          ref("middle_name"),
          " ",
          ref("first_surname"),
          " ",
          ref("second_surname")
        ),
        "like",
        `%${text}%`
      )
      .withGraphFetched({ publication: true, rol: true });

    return { result: user };
  };

  findPermissions = async (userId) => {
    const {
      rol: { options },
    } = await User.query().findById(userId).withGraphFetched("rol.*");
    return options.map((o) => o.route);
  };

  verifyCredentials = async (username, password) => {
    const result = await User.query().findOne({ username });

    if (!result) {
      return false;
    }

    const passwordValid = await bcrypt.compare(password, result.password);

    if (!passwordValid) {
      return false;
    }

    return result;
  };

  async $beforeInsert(queryContext) {
    await super.$beforeInsert(queryContext);
    this.created_at = new Date();
  }

  async $beforeUpdate(opt, queryContext) {
    await super.$beforeInsert(opt, queryContext);
    this.updated_at = new Date();
  }
}

module.exports = User;
