const { Model, fn, ref } = require("objection");

class EmergencyContact extends Model {
  static get idColumn() {
    return "id";
  }

  static get tableName() {
    return "emergency_contact";
  }

  static get jsonSchema() {
    return {
      type: "object",
      required: ["first_name", "last_name", "parentage", "cellphone"],
      properties: {
        first_name: { type: "string" },
        last_name: { type: "string" },
        parentage: { type: "string" },
        cellphone: { type: "string" },
      },
    };
  }

  static get relationMappings() {
    const Patient = require("./patientModel");

    return {
      patients: {
        relation: Model.HasManyRelation,
        modelClass: Patient,

        join: {
          from: "emergency_contact.id",
          to: "patients.emergency_contact_id",
        },
      },
    };
  }

  fetchAll = async (page = "", limit = "", text = "") => {
    if (parseInt(page) > 0 && parseInt(limit) > 0) {
      const { results, total } = await EmergencyContact.query()
        .where("first_name", "like", `%${text}%`)
        .orWhere("last_name", "like", `%${text}%`)
        .orWhere("parentage", "like", `%${text}%`)
        .orWhere("cellphone", "like", `%${text}%`)
        .orWhere(
          fn.concat(ref("first_name"), " ", ref("last_name")),
          "like",
          `%${text}%`
        )
        .page(page - 1, limit)
        .withGraphFetched({ patients: true });

      const pageNumbers = Math.ceil(total / limit);

      return { result: results, pageNumbers };
    }

    const emergencyContact = await EmergencyContact.query()
      .where("first_name", "like", `%${text}%`)
      .orWhere("last_name", "like", `%${text}%`)
      .orWhere("parentage", "like", `%${text}%`)
      .orWhere("cellphone", "like", `%${text}%`)
      .orWhere(
        fn.concat(ref("first_name"), " ", ref("last_name")),
        "like",
        `%${text}%`
      )
      .withGraphFetched({ patients: true });

    return { result: emergencyContact };
  };

  create = async (data) => {
    try {
      const response = await EmergencyContact.query().insert(data);
      return {
        result: true,
        data: response,
      };
    } catch (error) {
      return {
        result: false,
        data: {},
      };
    }
  };

  update = async (id, data) => {
    try {
      await EmergencyContact.query().findById(id).patch(data);
      return true;
    } catch (error) {
      return false;
    }
  };

  delete = async (id) => {
    try {
      await EmergencyContact.query().findById(id).patch({
        is_active: 0,
        deleted_at: new Date(),
      });

      return true;
    } catch (error) {
      return false;
    }
  };

  find = async (id) => {
    const result = await EmergencyContact.query().findById(id);

    return result;
  };

  async $beforeInsert(queryContext) {
    await super.$beforeInsert(queryContext);
    this.created_at = new Date();
  }

  async $beforeUpdate(opt, queryContext) {
    await super.$beforeInsert(opt, queryContext);
    this.updated_at = new Date();
  }
}

module.exports = EmergencyContact;
