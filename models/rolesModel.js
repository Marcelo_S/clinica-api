const { Model } = require("objection");
const RouteModel = require("./routeModel");

class Role extends Model {
  static get idColumn() {
    return "id";
  }

  static get tableName() {
    return "roles";
  }

  static get jsonSchema() {
    return {
      type: "object",
      required: ["description"],

      properties: {
        description: { type: "string" },
      },
    };
  }

  static relationMappings = {
    options: {
      relation: Model.ManyToManyRelation,
      modelClass: RouteModel,
      join: {
        from: "roles.id",
        through: {
          from: "route_role.id_role",
          to: "route_role.id_route",
        },
        to: "routes.id",
      },
    },
  };

  fetchAll = async (page = "", limit = "", text = "") => {
    if (parseInt(page) > 0 && parseInt(limit) > 0) {
      const { results, total } = await Role.query()
        .where("description", "like", `%${text}%`)
        .page(page - 1, limit)
        .withGraphFetched("options");

      console.log(text);

      const pageNumbers = Math.ceil(total / limit);

      return { result: results, pageNumbers };
    }

    const roles = await Role.query()
      .where("description", "like", `%${text}%`)
      .withGraphFetched("options");
    return { result: roles };
  };

  create = async (data) => {
    try {
      await Role.query().insert(data);
      return true;
    } catch (error) {
      return false;
    }
  };

  update = async (id, data) => {
    try {
      await Role.query().findById(id).patch(data);
      return true;
    } catch (error) {
      return false;
    }
  };

  delete = async (id) => {
    try {
      await Role.query().findById(id).patch({
        is_active: 0,
        deleted_at: new Date(),
      });
      return true;
    } catch (error) {
      return false;
    }
  };

  find = async (id) => {
    try {
      const result = await Role.query()
        .findById(id)
        .withGraphFetched("options");
      return result;
    } catch (error) {
      return false;
    }
  };

  addOption = async (optionId, rolId) => {
    try {
      await Role.relatedQuery("options").for(rolId).relate(optionId);
      return true;
    } catch (error) {
      return false;
    }
  };

  search = async (text) => {
    try {
      const result = await Role.query()
        .where("description", "like", `%${text}%`)
        .withGraphFetched("options");

      return result;
    } catch (error) {
      return false;
    }
  };

  async $beforeInsert(queryContext) {
    await super.$beforeInsert(queryContext);
    this.created_at = new Date();
  }

  async $beforeUpdate(opt, queryContext) {
    await super.$beforeInsert(opt, queryContext);
    this.updated_at = new Date();
  }
}

module.exports = Role;
