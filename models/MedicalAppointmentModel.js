const { Model } = require("objection");

class MedicalAppointment extends Model {
  static get idColumn() {
    return "id";
  }

  static get tableName() {
    return "medical_appointment";
  }

  static get jsonSchema() {
    return {
      type: "object",
      required: [
        "date_request",
        "date_appointment",
        "interval_time",
        "description",
        "subject",
        "patient_id",
      ],
      properties: {
        date_request: { type: "date" },
        date_appointment: { type: "date" },
        interval_time: { type: "integer" },
        description: { type: "string" },
        description: { type: "subject" },
        is_cancel: { type: "boolean" },
        is_active: { type: "boolean" },
        patient_id: { type: "integer" },
      },
    };
  }

  static get relationMappings() {
    const Patient = require("./patientModel");

    return {
      patient: {
        relation: Model.BelongsToOneRelation,
        modelClass: Patient,

        join: {
          from: "medical_appointment.patient_id",
          to: "patients.id",
        },
      },
    };
  }

  fetchAll = async (page = "", limit = "", { initialDate, endDate }) => {
    if (parseInt(page) > 0 && parseInt(limit) > 0) {
      const { results, total } = await MedicalAppointment.query()
        .where("date_appointment", ">=", initialDate || "")
        .orWhere("date_appointment", "<", endDate || initialDate || "")
        .page(page - 1, limit)
        .withGraphFetched({ patient: true });

      const pageNumbers = Math.ceil(total / limit);

      return { result: results, pageNumbers };
    }

    const medicalAppointment = await MedicalAppointment.query()
      .where("date_appointment", ">=", initialDate || "")
      .orWhere("date_appointment", "<", endDate || initialDate || "")
      .withGraphFetched({ patient: true });

    return { result: medicalAppointment };
  };

  create = async (data) => {
    try {
      await MedicalAppointment.query().insert(data);
      return true;
    } catch (error) {
      console.log({
        error,
      });
      return false;
    }
  };

  update = async (id, data) => {
    try {
      await MedicalAppointment.query().findById(id).patch(data);
      return true;
    } catch (error) {
      return false;
    }
  };

  delete = async (id) => {
    try {
      await MedicalAppointment.query().findById(id).patch({
        is_active: 0,
        deleted_at: new Date(),
      });

      return true;
    } catch (error) {
      return false;
    }
  };

  find = async (id) => {
    const result = await MedicalAppointment.query()
      .findById(id)
      .withGraphFetched({ patient: true });

    return result;
  };
}

module.exports = MedicalAppointment;
