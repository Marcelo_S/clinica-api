const express = require("express");
const Option = require("../models/routeModel");
const asyncWrapper = require("../middleware/asyncWrap");

const router = express.Router();

/**
 * @swagger
 *  definitions:
 *      route:
 *          type: object
 *          required:
 *            - description
 *            - route
 *          properties:
 *              description:
 *                  type: string
 *              route:
 *                  type: string
 *              is_active:
 *                  type: boolean
 */

/**
 * @swagger
 * /options:
 *     get:
 *         tags:
 *             - Routes
 *         description: List of routes
 *         produces:
 *             - application/json
 *         responses:
 *             200:
 *                 description: List of routes
 */

router.get(
  "/",
  asyncWrapper(async (req, res) => {
    const option = new Option();
    const { page, limit, text } = req.query;

    const { result, pageNumbers } = await option.fetchAll(page, limit, text);

    if (result.length) {
      return res.status(200).send({ data: result, pageNumbers });
    }

    return res.status(404).send({ error: "not found" });
  })
);

/**
 * @swagger
 * /options/{id}:
 *   get:
 *     tags:
 *       - Routes
 *     description: return a specific route
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *         description: route id
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: route information
 *       404:
 *         description: route not found
 */

router.get(
  "/:id",
  asyncWrapper(async (req, res) => {
    const { id } = req.params;
    const option = new Option();
    const result = await option.find(id);

    if (result) {
      return res.status(200).send({ data: result });
    }
    return res.status(404).send({ error: "Not found" });
  })
);

/**
 * @swagger
 * /options/create:
 *   post:
 *     tags:
 *       - Routes
 *     description: Creates a new route
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: route
 *         description: rol route
 *         schema:
 *          $ref: '#/definitions/route'
 *     responses:
 *       200:
 *         description: Route created
 */

router.post(
  "/create",
  asyncWrapper(async (req, res) => {
    const option = new Option();
    const result = await option.create(req.body);

    if (result) {
      return res.status(200).send({ message: "Created" });
    }
    return res.status(422).send({ error: "Failed to save" });
  })
);

/**
 * @swagger
 * /options/update/{id}:
 *   post:
 *     tags:
 *       - Routes
 *     description: Returns a route updated
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *         description: route id
 *         required: true
 *         type: integer
 *       - in: body
 *         name: route
 *         description: route object
 *         schema:
 *          $ref: '#/definitions/route'
 *     responses:
 *       200:
 *         description: application updated
 */

router.post(
  "/update/:id",
  asyncWrapper(async (req, res) => {
    const option = new Option();
    const { id } = req.params;
    const result = await option.update(id, req.body);

    if (result) {
      return res.status(200).send({ message: "updated succesful" });
    }
    return res.status(422).send({ error: "Falied to update" });
  })
);

/**
 * @swagger
 * /options/delete/{id}:
 *   post:
 *     tags:
 *       - Routes
 *     description: Deletes a single route, this record cannot be recovered
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *         description: route id
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: application deleted successfully
 */

router.post(
  "/delete/:id",
  asyncWrapper(async (req, res) => {
    const option = new Option();
    const { id } = req.params;
    const result = await option.delete(id);

    if (result) {
      return res.status(200).send({ message: "record removed" });
    }

    return res.status(404).send({ error: "failed to removed" });
  })
);

module.exports = router;
