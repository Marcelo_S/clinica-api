const express = require("express");
const MedicalAppointment = require("../models/MedicalAppointmentModel");
const asyncWrapper = require("../middleware/asyncWrap");

const router = express.Router();

/**
 * @swagger
 *  definitions:
 *      medical_appointment:
 *          type: object
 *          required:
 *            - date_request
 *            - date_appointment
 *            - interval_time
 *            - description
 *            - subject
 *            - patient_id
 *          properties:
 *              date_request:
 *                  type: string
 *              date_appointment:
 *                  type: string
 *              subject:
 *                  type: string
 *              interval_time:
 *                  type: integer
 *              patient_id:
 *                  type: integer
 *              description:
 *                  type: string
 *              is_cancel:
 *                  type: boolean
 *              is_active:
 *                  type: boolean
 */

/**
 * @swagger
 * /medical-appointments:
 *     get:
 *         tags:
 *             - MedicalAppointments
 *         description: List of medical appointment
 *         produces:
 *             - application/json
 *
 *         parameters:
 *             - in: path
 *               name: page
 *               description: number of page
 *               type: integer
 *             - in: path
 *               name: limit
 *               description: limit of record
 *               type: integer
 *             - in: path
 *               name: starDate
 *               description: start date to filter
 *               type: string
 *             - in: path
 *               name: endDate
 *               description: end date to filter
 *               type: string
 *         responses:
 *             200:
 *                 description: List of medical appointment
 */

router.get(
  "/",
  asyncWrapper(async (req, res) => {
    const medicalA = new MedicalAppointment();
    const { page, limit, startDate, endDate } = req.query;
    const { result, pageNumbers } = await medicalA.fetchAll(page, limit, {
      startDate,
      endDate,
    });

    if (result.length) {
      return res.status(200).send({ data: result, pageNumbers });
    }

    return res.status(404).send({ error: "not found" });
  })
);

/**
 * @swagger
 * /medical-appointments/{id}:
 *   get:
 *     tags:
 *       - MedicalAppointments
 *     description: return a medical appointment
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *         description: medical appointment id
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: medical appointment information
 *       404:
 *         description: medical appointment not found
 */

router.get(
  "/:id",
  asyncWrapper(async (req, res) => {
    const { id } = req.params;
    const medicalA = new MedicalAppointment();
    const result = await medicalA.find(id);

    if (result) {
      return res.status(200).send({ data: result });
    }
    return res.status(404).send({ error: "Not found" });
  })
);

/**
 * @swagger
 * /medical-appointments/create:
 *   post:
 *     tags:
 *       - MedicalAppointments
 *     description: Creates a medical appointment
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: medical appointment
 *         description: medical appointment object
 *         schema:
 *          $ref: '#/definitions/medical_appointment'
 *     responses:
 *       200:
 *         description: medical appointment created
 */

router.post(
  "/create",
  asyncWrapper(async (req, res) => {
    const medicalA = new MedicalAppointment();
    const result = await medicalA.create(req.body);

    if (result) {
      return res.status(200).send({ message: "Created" });
    }

    return res.status(422).send({ error: "Failed to save" });
  })
);

/**
 * @swagger
 * /medical-appointments/update/{id}:
 *   post:
 *     tags:
 *       - MedicalAppointments
 *     description: Returns a medical appoint updated updated
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *         description: medical appointment id
 *         required: true
 *         type: integer
 *       - in: body
 *         name: medical appointment
 *         description: medical appointment object
 *         schema:
 *          $ref: '#/definitions/medical_appointment'
 *     responses:
 *       200:
 *         description: medical appointment updated
 */

router.post(
  "/update/:id",
  asyncWrapper(async (req, res) => {
    const medicalA = new MedicalAppointment();
    const { id } = req.params;
    const result = await medicalA.update(id, req.body);

    if (result) {
      return res.status(200).send({ message: "updated succesful" });
    }
    return res.status(422).send({ error: "Falied to update" });
  })
);

/**
 * @swagger
 * /medical-appointments/delete/{id}:
 *   post:
 *     tags:
 *       - MedicalAppointments
 *     description: Deletes a single medical appointment, this record cannot be recovered
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *         description: medical appointment id
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: medical appointment deleted successfully
 */

router.post(
  "/delete/:id",
  asyncWrapper(async (req, res) => {
    const medicalA = new MedicalAppointment();
    const { id } = req.params;
    const result = await medicalA.delete(id);

    if (result) {
      return res.status(200).send({ message: "record removed" });
    }

    return res.status(404).send({ error: "failed to removed" });
  })
);

module.exports = router;
