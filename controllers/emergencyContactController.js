const express = require("express");
const EmergencyContact = require("../models/emergencyContactModel");
const asyncWrapper = require("../middleware/asyncWrap");

const router = express.Router();

/**
 * @swagger
 *  definitions:
 *      emergency_contact:
 *          type: object
 *          required:
 *            - first_name
 *            - last_name
 *            - parentage
 *            - cellphone
 *          properties:
 *              first_name:
 *                  type: string
 *              last_name:
 *                  type: string
 *              parentage:
 *                  type: string
 *              cellphone:
 *                  type: string
 *              is_active:
 *                  type: boolean
 */

/**
 * @swagger
 * /emergency-contact:
 *     get:
 *         tags:
 *             - Emergency_Contact
 *         description: List of emergency contact
 *         produces:
 *             - application/json
 *         responses:
 *             200:
 *                 description: List of emergency contact
 */

router.get(
  "/",
  asyncWrapper(async (req, res) => {
    const emergencyC = new EmergencyContact();
    const { page, limit, text } = req.query;
    const { result, pageNumbers } = await emergencyC.fetchAll(
      page,
      limit,
      text
    );

    if (result.length) {
      return res.status(200).send({ data: result, pageNumbers });
    }

    return res.status(404).send({ error: "not found" });
  })
);

/**
 * @swagger
 * /emergency-contact/{id}:
 *   get:
 *     tags:
 *       - Emergency_Contact
 *     description: return a specific emergency contact
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *         description: emergency contact id
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: emergency contact information
 *       404:
 *         description: emergency contact not found
 */

router.get(
  "/:id",
  asyncWrapper(async (req, res) => {
    const { id } = req.params;
    const emergencyC = new EmergencyContact();
    const result = await emergencyC.find(id);

    if (result) {
      return res.status(200).send({ data: result });
    }
    return res.status(404).send({ error: "Not found" });
  })
);

/**
 * @swagger
 * /emergency-contact/create:
 *   post:
 *     tags:
 *       - Emergency_Contact
 *     description: Creates a new emergency contact
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: emergency contact
 *         description: rol emergency contact
 *         schema:
 *          $ref: '#/definitions/emergency_contact'
 *     responses:
 *       200:
 *         description: emergency contact created
 */

router.post(
  "/create",
  asyncWrapper(async (req, res) => {
    const emergencyC = new EmergencyContact();
    const { data, result } = await emergencyC.create(req.body);

    if (result) {
      return res.status(200).send({ message: "Created", result: data });
    }
    return res.status(422).send({ error: "Failed to save" });
  })
);

/**
 * @swagger
 * /emergency-contact/update/{id}:
 *   post:
 *     tags:
 *       - Emergency_Contact
 *     description: Returns a emergency contact updated
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *         description: emergency contact id
 *         required: true
 *         type: integer
 *       - in: body
 *         name: emergency contact
 *         description: emergency contact object
 *         schema:
 *          $ref: '#/definitions/emergency_contact'
 *     responses:
 *       200:
 *         description: emergency contact updated
 */

router.post(
  "/update/:id",
  asyncWrapper(async (req, res) => {
    const emergency = new EmergencyContact();
    const { id } = req.params;
    const result = await emergency.update(id, req.body);

    if (result) {
      return res.status(200).send({ message: "updated succesful" });
    }
    return res.status(422).send({ error: "Falied to update" });
  })
);

/**
 * @swagger
 * /emergency-contact/delete/{id}:
 *   post:
 *     tags:
 *       - Emergency_Contact
 *     description: Deletes a single emergency contact, this record cannot be recovered
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *         description: emergency contact id
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: application deleted successfully
 */

router.post(
  "/delete/:id",
  asyncWrapper(async (req, res) => {
    const emergencyC = new EmergencyContact();
    const { id } = req.params;
    const result = await emergencyC.delete(id);

    if (result) {
      return res.status(200).send({ message: "record removed" });
    }

    return res.status(404).send({ error: "failed to removed" });
  })
);
module.exports = router;
