const express = require("express");
const RoutesRole = require("../models/routesRoleModel");
const asyncWrapper = require("../middleware/asyncWrap");

const router = express.Router();

router.get(
  "/",
  asyncWrapper(async (_req, res) => {
    const routesRole = new RoutesRole();
    const result = await routesRole.fetchAll();

    if (result.length) {
      return res.status(200).send({ data: result });
    }

    return res.status(404).send({ error: "not found" });
  })
);

router.get(
  "/:id",
  asyncWrapper(async (req, res) => {
    const { id } = req.params;
    const RoutesRole = new RoutesRole();
    const result = await RoutesRole.find(id);
    if (result) {
      return res.status(200).send({ data: result });
    }
    return res.status(404).send({ error: "Not found" });
  })
);

router.post(
  "/create",
  asyncWrapper(async (req, res) => {
    const RoutesRole = new RoutesRole();
    const result = await RoutesRole.create(req.body);

    if (result) {
      return res.status(200).send({ message: "Created" });
    }

    return res.status(422).send({ error: "Failed to save" });
  })
);

router.post(
  "/update/:id",
  asyncWrapper(async (req, res) => {
    const RoutesRole = new RoutesRole();
    const { id } = req.params;
    const result = await RoutesRole.update(id, req.body);

    if (result) {
      return res.status(200).send({ message: "updated succesful" });
    }

    return res.status(422).send({ error: "Falied to update" });
  })
);

router.post(
  "/delete/:id",
  asyncWrapper(async (req, res) => {
    const RoutesRole = new RoutesRole();
    const { id } = req.params;
    const result = await RoutesRole.delete(id);

    if (result) {
      return res.status(200).send({ message: "record removed" });
    }

    return res.status(404).send({ error: "failed to removed" });
  })
);

module.exports = router;
