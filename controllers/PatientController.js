const express = require("express");
const Patient = require("../models/patientModel");
const asyncWrapper = require("../middleware/asyncWrap");

const router = express.Router();

/**
 * @swagger
 *  definitions:
 *      patient:
 *          type: object
 *          required:
 *            - first_name
 *            - last_name
 *            - age
 *            - birthday
 *            - gender
 *            - address
 *            - city
 *            - cellphone
 *            - mail_address
 *            - martial_status
 *            - emergency_contact_id
 *          properties:
 *              first_name:
 *                  type: string
 *              last_name:
 *                  type: string
 *              age:
 *                  type: string
 *              birthday:
 *                  type: string
 *              gender:
 *                  type: boolean
 *              address:
 *                  type: string
 *              city:
 *                  type: string
 *              cellphone:
 *                  type: string
 *              mail_address:
 *                  type: string
 *              martial_status:
 *                  type: string
 *              profession:
 *                  type: string
 *              emergency_contact_id:
 *                  type: integer
 *              is_active:
 *                  type: boolean
 */

/**
 * @swagger
 * /patients:
 *     get:
 *         tags:
 *             - Patients
 *         description: List of patients
 *         produces:
 *             - application/json
 *         responses:
 *             200:
 *                 description: List of patients
 */

router.get(
  "/",
  asyncWrapper(async (req, res) => {
    const patient = new Patient();
    const { page, limit, text } = req.query;
    const { result, pageNumbers } = await patient.fetchAll(page, limit, text);

    if (result.length) {
      return res.status(200).send({ data: result, pageNumbers });
    }

    return res.status(404).send({ error: "not found" });
  })
);

/**
 * @swagger
 * /patients/{id}:
 *   get:
 *     tags:
 *       - Patients
 *     description: return a specific patient
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *         description: patient id
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: patient information
 *       404:
 *         description: patient not found
 */

router.get(
  "/:id",
  asyncWrapper(async (req, res) => {
    const { id } = req.params;
    const patient = new Patient();
    const result = await patient.find(id);

    if (result) {
      return res.status(200).send({ data: result });
    }
    return res.status(404).send({ error: "Not found" });
  })
);

/**
 * @swagger
 * /patients/create:
 *   post:
 *     tags:
 *       - Patients
 *     description: Creates a new patient
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: patient
 *         description: patient object
 *         schema:
 *          $ref: '#/definitions/patient'
 *     responses:
 *       200:
 *         description: patient created
 */

router.post(
  "/create",
  asyncWrapper(async (req, res) => {
    const patient = new Patient();
    const result = await patient.create(req.body);

    if (result) {
      return res.status(200).send({ message: "Created" });
    }
    return res.status(422).send({ error: "Failed to save" });
  })
);

/**
 * @swagger
 * /patients/update/{id}:
 *   post:
 *     tags:
 *       - Patients
 *     description: Returns a patient updated
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *         description: patient id
 *         required: true
 *         type: integer
 *       - in: body
 *         name: patient
 *         description: patient object
 *         schema:
 *          $ref: '#/definitions/patient'
 *     responses:
 *       200:
 *         description:patient updated
 */

router.post(
  "/update/:id",
  asyncWrapper(async (req, res) => {
    const patient = new Patient();
    const { id } = req.params;
    const result = await patient.update(id, req.body);

    if (result) {
      return res.status(200).send({ message: "updated succesful" });
    }
    return res.status(422).send({ error: "Falied to update" });
  })
);

/**
 * @swagger
 * /patients/delete/{id}:
 *   post:
 *     tags:
 *       - Patients
 *     description: Deletes a single patient, this record cannot be recovered
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *         description: patient id
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: patient deleted successfully
 */

router.post(
  "/delete/:id",
  asyncWrapper(async (req, res) => {
    const patient = new Patient();
    const { id } = req.params;
    const result = await patient.delete(id);

    if (result) {
      return res.status(200).send({ message: "record removed" });
    }

    return res.status(404).send({ error: "failed to removed" });
  })
);
module.exports = router;
