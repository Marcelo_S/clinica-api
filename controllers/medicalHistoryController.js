const express = require("express");
const MedicalHistory = require("../models/MedicalHistoryModel");
const asyncWrapper = require("../middleware/asyncWrap");

const router = express.Router();

/**
 * @swagger
 *  definitions:
 *      medical_history:
 *          type: object
 *          required:
 *            - reason_admission
 *            - health_condition
 *            - patient_id
 *          properties:
 *              reason_admission:
 *                  type: string
 *              health_condition:
 *                  type: string
 *              patient_id:
 *                  type: integer
 *              is_active:
 *                  type: boolean
 */

/**
 * @swagger
 * /medical-histories:
 *     get:
 *         tags:
 *             - MedicalHistories
 *         description: List of medical history
 *         produces:
 *             - application/json
 *
 *         parameters:
 *             - in: query
 *               name: page
 *               description: number of page
 *               type: integer
 *             - in: query
 *               name: limit
 *               description: limit of record
 *               type: integer
 *         responses:
 *             200:
 *                 description: List of medical history
 */

router.get(
  "/",
  asyncWrapper(async (req, res) => {
    const medicalH = new MedicalHistory();
    const { page, limit } = req.query;
    const { result, pageNumbers } = await medicalH.fetchAll(page, limit);

    if (result.length) {
      return res.status(200).send({ data: result, pageNumbers });
    }

    return res.status(404).send({ error: "not found" });
  })
);

/**
 * @swagger
 * /medical-histories/{id}:
 *   get:
 *     tags:
 *       - MedicalHistories
 *     description: return a medical history
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *         description: medical history id
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: medical history information
 *       404:
 *         description: medical history not found
 */

router.get(
  "/:id",
  asyncWrapper(async (req, res) => {
    const { id } = req.params;
    const medicalH = new MedicalHistory();
    const result = await medicalH.find(id);

    if (result) {
      return res.status(200).send({ data: result });
    }
    return res.status(404).send({ error: "Not found" });
  })
);

/**
 * @swagger
 * /medical-histories/create:
 *   post:
 *     tags:
 *       - MedicalHistories
 *     description: Creates a medical history
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: medical history
 *         description: medical history object
 *         schema:
 *          $ref: '#/definitions/medical_history'
 *     responses:
 *       200:
 *         description: medical history created
 */

router.post(
  "/create",
  asyncWrapper(async (req, res) => {
    const medicalH = new MedicalHistory();
    const result = await medicalH.create(req.body);

    if (result) {
      return res.status(200).send({ message: "Created" });
    }

    return res.status(422).send({ error: "Failed to save" });
  })
);

/**
 * @swagger
 * /medical-histories/update/{id}:
 *   post:
 *     tags:
 *       - MedicalHistories
 *     description: Returns a medical history updated
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *         description: medical history id
 *         required: true
 *         type: integer
 *       - in: body
 *         name: medical history
 *         description: medical history object
 *         schema:
 *          $ref: '#/definitions/medical_history'
 *     responses:
 *       200:
 *         description: medical history updated
 */

router.post(
  "/update/:id",
  asyncWrapper(async (req, res) => {
    const medicalH = new MedicalHistory();
    const { id } = req.params;
    const result = await medicalH.update(id, req.body);

    if (result) {
      return res.status(200).send({ message: "updated succesful" });
    }
    return res.status(422).send({ error: "Falied to update" });
  })
);

/**
 * @swagger
 * /medical-histories/delete/{id}:
 *   post:
 *     tags:
 *       - MedicalHistories
 *     description: Deletes a single medical history, this record cannot be recovered
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *         description: medical history id
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: medical history deleted successfully
 */

router.post(
  "/delete/:id",
  asyncWrapper(async (req, res) => {
    const medicalH = new MedicalHistory();
    const { id } = req.params;
    const result = await medicalH.delete(id);

    if (result) {
      return res.status(200).send({ message: "record removed" });
    }

    return res.status(404).send({ error: "failed to removed" });
  })
);

module.exports = router;
