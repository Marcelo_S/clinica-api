const express = require("express");
const Roles = require("../models/rolesModel");
const asyncWrapper = require("../middleware/asyncWrap");
const roleSchema = require("../schema/roleSchema");
const validator = require("../middleware/schemaValidator");

const router = express.Router();

/**
 * @swagger
 *  definitions:
 *      role:
 *          type: object
 *          required:
 *            - description
 *          properties:
 *              description:
 *                  type: string
 *              is_active:
 *                  type: boolean
 */

/**
 * @swagger
 *  definitions:
 *      role_option:
 *          type: object
 *          required:
 *            - roleId
 *            - optionId
 *          properties:
 *              optionId:
 *                  type: integer
 *              roleId:
 *                  type: integer
 */

/**
 * @swagger
 * /roles:
 *     get:
 *         tags:
 *             - Roles
 *         description: List of roles and options
 *         produces:
 *             - application/json
 *         responses:
 *             200:
 *                 description: List of roles and permissions
 */

router.get(
  "/",
  asyncWrapper(async (req, res) => {
    const roles = new Roles();
    const { page, limit, text } = req.query;
    const { result, pageNumbers } = await roles.fetchAll(page, limit, text);

    if (result.length) {
      return res.status(200).send({ data: result, pageNumbers });
    }

    return res.status(404).send({ error: "not found" });
  })
);

/**
 * @swagger
 * /roles/{id}:
 *   get:
 *     tags:
 *       - Roles
 *     description: return a specific role
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *         description: rol id
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: rol information
 *       404:
 *         description: rol not found
 */
router.get(
  "/:id",
  asyncWrapper(async (req, res) => {
    const { id } = req.params;
    const roles = new Roles();
    const result = await roles.find(id);

    if (result) {
      return res.status(200).send({ data: result });
    }
    return res.status(404).send({ error: "Not found" });
  })
);

/**
 * @swagger
 * /roles/create:
 *   post:
 *     tags:
 *       - Roles
 *     description: Creates a new role
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: role
 *         description: rol object
 *         schema:
 *          $ref: '#/definitions/role'
 *     responses:
 *       200:
 *         description: Role created
 */

router.post(
  "/create",
  validator(roleSchema.roleCreation),
  asyncWrapper(async (req, res) => {
    const roles = new Roles();

    const result = await roles.create(req.body);
    if (result) {
      return res.status(200).send({ message: "Created" });
    }

    return res.status(422).send({ error: "Failed to save" });
  })
);

/**
 * @swagger
 * /roles/update/{id}:
 *   post:
 *     tags:
 *       - Roles
 *     description: Returns a role updated
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *         description: role id
 *         required: true
 *         type: integer
 *       - in: body
 *         name: role
 *         description: role object
 *         schema:
 *          $ref: '#/definitions/role'
 *     responses:
 *       200:
 *         description: application updated
 */

router.post(
  "/update/:id",
  asyncWrapper(async (req, res) => {
    const roles = new Roles();
    const { id } = req.params;

    const result = await roles.update(id, req.body);

    if (result) {
      return res.status(200).send({ message: "updated successfully" });
    }

    return res.status(422).send({ error: "Failed to update" });
  })
);

/**
 * @swagger
 * /roles/delete/{id}:
 *   post:
 *     tags:
 *       - Roles
 *     description: Deletes a single role, this record cannot be recovered
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *         description: Role id
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: application deleted successfully
 */

router.post(
  "/delete/:id",
  asyncWrapper(async (req, res) => {
    const roles = new Roles();
    const { id } = req.params;
    const result = await roles.delete(id);

    if (result) {
      return res.status(200).send({ message: "record removed" });
    }
    return res.status(404).send({ error: "failed to removed" });
  })
);

/**
 * @swagger
 * /roles/options:
 *   post:
 *     tags:
 *       - Roles
 *     description: Relationship between role and option
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: rol
 *         description: rol relationship object
 *         schema:
 *          $ref: "#/definitions/role_option"
 *     responses:
 *       200:
 *         description: Rol created
 */

router.post(
  "/options",
  asyncWrapper(async (req, res) => {
    const { rolId, optionId } = req.body;
    const roles = new Roles();

    const success = roles.addOption(optionId, rolId);

    if (success) {
      return res.status(200).send({ message: "Option added" });
    }

    return res.status(422).send({ error: "Failed to save" });
  })
);

/**
 * @swagger
 * /roles/search/{text}:
 *   get:
 *     tags:
 *       - Roles
 *     description: returns an array of roles with search hits
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: text
 *         description: search text
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: application deleted successfully
 */

router.get(
  "/search/:text",
  asyncWrapper(async (req, res) => {
    const roles = new Roles();
    const { text } = req.params;

    const result = await roles.search(text);

    if (result.length) {
      return res.status(200).send({ data: result });
    }

    return res.status(404).send({ error: "Not found" });
  })
);

module.exports = router;
