exports.up = function (knex) {
  return knex.schema.createTable("routes", (table) => {
    table.increments("id").primary();
    table.text("description").notNullable();
    table.text("route").notNullable();
    table.boolean("is_active").defaultTo(true);
    // control fields
    table.timestamps();
    table.timestamp("deleted_at").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTableIfExists("routes");
};
