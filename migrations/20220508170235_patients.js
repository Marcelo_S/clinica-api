exports.up = function (knex) {
  return knex.schema.createTable("patients", (table) => {
    table.increments("id").primary();
    table.text("first_name").notNullable();
    table.text("last_name").notNullable();
    table.text("age").notNullable();
    table.datetime("birthday").notNullable();
    table.boolean("gender").defaultTo(true);
    table.text("address").notNullable();
    table.text("city").notNullable();
    table.text("cellphone").notNullable();
    table.text("mail_address").notNullable();
    table.text("martial_status").notNullable();
    table.text("profession");

    // foreign keys
    table.integer("emergency_contact_id");

    table.boolean("is_active").defaultTo(true);
    table.timestamps();
    table.timestamp("deleted_at").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTableIfExists("patients");
};
