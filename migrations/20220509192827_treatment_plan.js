exports.up = function (knex) {
  return knex.schema.createTable("treatment_plan", (table) => {
    table.increments("id").primary();
    table.datetime("start_date").notNullable();
    table.datetime("end_date").notNullable();
    table.text("description").notNullable();

    // foreign key
    table.integer("medical_history_id");

    table.boolean("is_active").defaultTo(true);
    table.timestamps();
    table.timestamp("deleted_at").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTableIfExists("treatment_plan");
};
