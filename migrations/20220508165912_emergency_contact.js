exports.up = function (knex) {
  return knex.schema.createTable("emergency_contact", (table) => {
    table.increments("id").primary();
    table.text("first_name").notNullable();
    table.text("last_name").notNullable();
    table.text("parentage").notNullable();
    table.text("cellphone").notNullable();

    table.boolean("is_active").defaultTo(true);
    table.timestamps();
    table.timestamp("deleted_at").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTableIfExists("emergency_contact");
};
