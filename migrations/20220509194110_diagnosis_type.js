exports.up = function (knex) {
  return knex.schema.createTable("diagnosis_type", (table) => {
    table.increments("id").primary();
    table.text("name").notNullable();
    table.text("description").notNullable();

    // control fields
    table.boolean("is_active").defaultTo(true);
    table.timestamps();
    table.timestamp("deleted_at").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTableIfExists("diagnosis_type");
};
