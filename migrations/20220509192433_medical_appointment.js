exports.up = function (knex) {
  return knex.schema.createTable("medical_appointment", (table) => {
    table.increments("id").primary();
    table.datetime("date_request").notNullable();
    table.datetime("date_appointment").notNullable();
    table.integer("interval_time").notNullable();
    table.text("subject").notNullable();
    table.text("description").notNullable();
    table.boolean("is_cancel").defaultTo(false);

    // foreign key
    table.integer("patient_id");

    // field control
    table.boolean("is_active").defaultTo(true);
    table.timestamps();
    table.timestamp("deleted_at").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTableIfExists("medical_appointment");
};
