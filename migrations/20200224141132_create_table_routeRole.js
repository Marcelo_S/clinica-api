exports.up = function (knex) {
  return knex.schema.createTable("route_role", (table) => {
    table.increments("id").primary();
    table.integer("id_role").notNullable();
    table.integer("id_route").notNullable();
    table.boolean("is_active").defaultTo(true);

    table.timestamps();
    table.timestamp("deleted_at").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTableIfExists("route_role");
};
