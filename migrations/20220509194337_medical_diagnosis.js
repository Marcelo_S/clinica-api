exports.up = function (knex) {
  return knex.schema.createTable("medical_diagnosis", (table) => {
    table.increments("id").primary();
    table.text("description").notNullable();
    table.text("note").notNullable();
    table.text("entered_by").notNullable();

    // foreign key
    table.integer("diagnosis_type_id");
    table.integer("medical_history_id");

    table.boolean("is_active").defaultTo(true);
    table.timestamps();
    table.timestamp("deleted_at").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTableIfExists("diagnosis_type");
};
