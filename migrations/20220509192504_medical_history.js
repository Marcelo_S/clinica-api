exports.up = function (knex) {
  return knex.schema.createTable("medical_history", (table) => {
    table.increments("id").primary();
    table.text("identifier").notNullable();
    table.text("reason_admission").notNullable();
    table.text("health_condition").notNullable();

    // foreign key
    table.integer("patient_id");

    table.boolean("is_active").defaultTo(true);
    table.timestamps();
    table.timestamp("deleted_at").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTableIfExists("medical_history");
};
