exports.up = function (knex) {
  return knex.schema.createTable("medication", (table) => {
    table.increments("id").primary();
    table.text("name").notNullable();
    table.text("side_effects").notNullable();
    table.text("dosage").notNullable();
    table.text("time_interval").notNullable();

    // foreign key
    table.integer("treatment_plan_id");

    table.boolean("is_active").defaultTo(true);
    table.timestamps();
    table.timestamp("deleted_at").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTableIfExists("medication");
};
