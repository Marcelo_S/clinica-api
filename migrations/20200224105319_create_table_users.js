exports.up = function (knex) {
  return knex.schema.createTable("users", (table) => {
    table.increments("id").primary();
    table.integer("id_rol").notNullable();
    table.text("first_name").notNullable();
    table.text("middle_name").notNullable();
    table.text("first_surname").notNullable();
    table.text("second_surname").notNullable();
    table.text("username").notNullable();
    table.text("password").notNullable();
    table.text("description").notNullable();
    table.boolean("is_active").defaultTo(true);
    table.timestamps();
    table.timestamp("deleted_at").nullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTableIfExists("users");
};
